import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  message: string = "hi";

  changeMessage(message: string) {
    this.message = message;
  }

}
