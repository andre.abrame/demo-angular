import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BindingsComponent } from './components/bindings/bindings.component';
import { ParentComponent } from './components/intercomponent-communication/parent/parent.component';
import { StructuralDirectivesComponent } from './components/structural-directives/structural-directives.component';
import { ComponentWithPathParamComponent } from './components/component-with-path-param/component-with-path-param.component';

const routes: Routes = [
  { path: "", component: HomeComponent},
  { path: "bindings", component: BindingsComponent},
  { path: "intercomponents-communication", component: ParentComponent},
  { path: "structural-directives", component: StructuralDirectivesComponent},
  { path: "pathparam/:num", component: ComponentWithPathParamComponent},
  { path: "**", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
