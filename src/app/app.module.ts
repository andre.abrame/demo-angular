import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BindingsComponent } from './components/bindings/bindings.component';
import { FormsModule } from '@angular/forms';
import { StructuralDirectivesComponent } from './components/structural-directives/structural-directives.component';
import { ParentComponent } from './components/intercomponent-communication/parent/parent.component';
import { ChildComponent } from './components/intercomponent-communication/child/child.component';
import { HomeComponent } from './components/home/home.component';
import { ComponentWithPathParamComponent } from './components/component-with-path-param/component-with-path-param.component';

@NgModule({
  declarations: [
    AppComponent,
    BindingsComponent,
    StructuralDirectivesComponent,
    ParentComponent,
    ChildComponent,
    HomeComponent,
    ComponentWithPathParamComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
