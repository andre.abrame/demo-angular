import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  templateUrl: './bindings.component.html',
  styleUrls: ['./bindings.component.css']
})
export class BindingsComponent implements OnInit {

  name: string = "andre";

  constructor() { }

  ngOnInit(): void {}

  onClick() {
    if (this.name === "andre")
      this.name = this.name.toUpperCase();
    else
      this.name = this.name.toLowerCase();
  }

}
