import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, AfterViewInit {

  @ViewChildren(ChildComponent) children?: ChildComponent;
  @ViewChild("title") title?: ElementRef;

  constructor() { }

  ngOnInit(): void {
    console.log(this.children)
  }

  ngAfterViewInit(): void {
    console.log(this.children)
    console.log(this.title)
  }

  onSayHi(name: string) {
    alert(`Hi ${name}`);
  }

}
