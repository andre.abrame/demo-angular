import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() name?: string;
  @Output() sayHi: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    public messageService: MessageService
  ) {}

  ngOnInit(): void {
  }

  onSayHi() {
    this.sayHi.emit(this.name);
  }

  onChangeMessage() {
    this.messageService.changeMessage("salut");
  }

}
