import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscription, map } from 'rxjs';

@Component({
  selector: 'app-component-with-path-param',
  templateUrl: './component-with-path-param.component.html',
  styleUrls: ['./component-with-path-param.component.css']
})
export class ComponentWithPathParamComponent implements OnInit {

  numSnapshot?: number;
  numObservable$?: Observable<number>;
  activatedRouteSubscription?: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.numSnapshot = this.activatedRoute.snapshot.params['num'];
    this.numObservable$ = this.activatedRoute.params.pipe(
      map(params => parseInt(params['num']))
    );
  }
}
