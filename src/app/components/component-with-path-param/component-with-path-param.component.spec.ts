import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentWithPathParamComponent } from './component-with-path-param.component';

describe('ComponentWithPathParamComponent', () => {
  let component: ComponentWithPathParamComponent;
  let fixture: ComponentFixture<ComponentWithPathParamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentWithPathParamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentWithPathParamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
